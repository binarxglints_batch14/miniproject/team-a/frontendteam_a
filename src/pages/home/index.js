import MovieCarousel from "../../components/carousel/MovieCarousel"
import MovieContainer from "../../components/movie-container/MovieContainer"




function Home() {
  return (
    <>
      <MovieCarousel />
      <MovieContainer />
    </>
  )
}

export default Home